# ND DORMS (Client Server Final Project)

Natalie Whalen (nwhalen2)

TO RUN: go into server/ folder & run 

        python3 server.py
        
        the site will be on localhost port = 51027
        then proceed to https://nwhalen2.gitlab.io/ClientServerFinal/jsfrontend/index.html

TO RUN TESTS: go into server/ folder & run

        python3 test_api.py

        python3 test_ws.py

PRESENTATION SLIDES: https://docs.google.com/presentation/d/1XbciRVl9vKGTdRmu9CylnJMNiOyjDcnlODgUiAS-ofs/edit?usp=sharing

DEMO VIDEO: https://drive.google.com/file/d/18TJ7iuDvXgaqhYDbxPxq3zkGy4AHzJP3/view?usp=sharing

SERVER CODE WALKTHROUGH: https://drive.google.com/file/d/1QOLrhMVrsjLApjx1MrLwR61LpdeNyRkZ/view?usp=sharing

TEST DEMO: https://drive.google.com/file/d/1O0IwYqy6MoAqVE0uDeNnhfLDD3NDkER8/view?usp=sharing

FRONTEND CODE WALKTHROUGH: https://drive.google.com/file/d/1Dond2X7Yl_0tRCo9EvfHyrgDfJ2igUfK/view?usp=sharing


DATA (dorm.dat)

The data source used for this project consists of information on the undergraduate dorms of the University of Notre Dame. It was manually typed and consists of six fields (id number, dorm name, year founded, gender housed, quad located and mascot represented by) separated by double dashes. For example, the first key is 1 - which correponds to the id of the first dorm in the alphabet, Alumni. The line in dorm.dat that corresponds to Alumni's information is found on the first line as follows:
            1--Alumni--1931--Male--South--Dawgs

---------------------------------------OO API(server)---------------------------------------

OO LIBRARY (dorm_library.py)

This library consists of several methods within the class _dorm_database used for manipulating information from the data file. It begins with load_dorms() which simply extracts the data as lists of their respective categories from dorm.dat and places them in a dictionary called dorm_info. The method get_dorms() returns all of this information. The method get_dorm() takes in the dorm's id as a parameter and returns its corresponding information. The method set_dorm() takes in the dorm's id as well as a list of the dorm's informative categories that is then assigned as a new dorm in the dorm_info dictionary. The method delete_dorm() takes in the dorm's id as a parameter and deletes that dorm as well as all of its corresponding information from the database.

TESTING (test_api.py)

Tests for the API confirm the functionality of the methods defined in the _dorm_database class: get_dorm, get_dorms, set_dorm, and delete_dorm. It performs these tests successfully by asserting equality over the data output of these methods (updated dorm_info from the class or the the direct return data from the function) and what is received by the server through requests.

------------------------------REST API SPECIFICATION (server)------------------------------

CONTROLLER (dorm_controller.py)

The controller consists of the event handlers connected to the server through a dispatcher. The handler GET_DORM takes the dorm's id as a parameter and passes it to _dorm_database's get_dorm method. It then extracts the desired information on that dorm from get_dorm and stores it in a dictionary that is then sent back to the server. The handler PUT_DORM adds a new dorm by extracting the information sent from the message body of the PUT request as well as the id from the URL. It then calls the set_dorm function from _dorm_database to append the dorm_info dictionary with the new information. The handler DELETE_DORM takes in the dorm's id as a parameter and calls _dorm_database's delete_dorm method to remove that dorm and its corresponding information from the database. The handler POST_INDEX is similar to PUT_DORM, but does not take the dorm's id as input since the new dorm is automatically appended to the end of the dorm dictionary. GET_INDEX works similarly to GET_DORM but runs a for loop that accounts for all dorms in the database to add to the dictionary that is sent to the server. DELETE_INDEX works just like DELETE_DORM but runs in a for loop to account for all dorms in the database and effectively deletes all of the information stored in the dictionary.

SERVER (server.py)

The server uses the cherrypy library to instance a dispatcher that connects the server to the event handlers in the controller (via its connect function). It also creates a _dorm_database object in order to access the methods associated in that class. The server uses HTTP requests GET, PUT, POST, and delete data from its stored dictionary. It also incorporates CORS as a method of security and configures the site on localhost port 51027.

TESTING (test_ws.py)

Tests for the web server confirm the functionality of the event handlers as defined in the controller: GET_DORM, PUT_DORM, DELETE_DORM, GET_INDEX, POST_INDEX, and DELETE_INDEX. It performs these tests successfully by asserting equality over what is received by the server through requests and what is expected of that data (i.e. I'll find the correct dorm name and its corresponding information where I expect it to be in the server dictionary).

------------------------------USER INTERACTION (jsfrontend)------------------------------

INDEX.HTML

Site webpage consists of a header of information, a photo of Notre Dame - that is updated to show an outside view of an individual dorm each time a user inputs a dorm name -, two buttons - one that shows all of the dorm names and one that clears that information -, an input field in which the user can type a dorm name, a button in which the user presses to see what options they have for dissecting dorm information such as year established, gender housed, quad located, and mascot represented by but also which presents a message if the user inputs a string that is not the name of one of ND's undergraduate dorms, and four buttons that correspond with what information the user wants to extract about the specified dorm, which each trigger an updated HTML that presents said information.

STYLE.CSS

This style sheet is very simple and holds merely a few specifications for what is presented in index.html.

MAIN.JS

This javascript file accounts for the user interaction with index.html and adjusts the visualization accordingly. It begins by listening for button clicks, namely the options button and the buttons that show and clear a list of all the dorm names. When the options button is clicked, getName() is called, which extracts the user input from the input field and stores it in a variable which is passed along to makeNetworkCallToDormApi(), the function that gets all of the dorm information from the server. If the show (all dorm names) button was pressed instead of the options button, the name variable was set to 'all' and makeNetworkCallToDormApi() passes the server information to showAll(), which sets the innerHTML of the element with id = 'dorm-names' equal to a list of all the dorm names. If the clear button is pressed, this element is cleared. If name is not equal to 'all', then getIDFromName() is called, which loops through the dorms and assigns an id according to the dorm name given by the user. If no dorm id is associated, an appropriate message is sent to the html element with id = 'not-a-dorm'. If a dorm id is found, then the program updates the image on the page to show a photo of the exterior of the corresponding building. It then assigns the innerHTML of each of the last four buttons to display properly to the user with showOptions(). It then listens for these buttons and adjusts the innerHTML of the element with id = 'response' with the desired information (year established, gender housed, quad located, or mascot represented by).

